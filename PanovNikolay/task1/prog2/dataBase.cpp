#include "dataBase.h"
std::vector<std::string> dataBase::nameCols;
int dataBase::callBackNameCols(void *NotUsed, int argc, char **argv, char **azColName)
{
   nameCols.clear();
   for (size_t i = 0; i < argc; i++) {
        nameCols.push_back(azColName[i]);
   }
   return 0;
}

int dataBase::callBackFindVal(void *NotUsed, int argc, char **argv, char **azColName)
{
    std::cout << argv[0] << std::endl;
    return 0;
}

int dataBase::callBackEmpty(void *NotUsed, int argc, char **argv, char **azColName)
{
    return 0;
}

void dataBase::open(std::string fileName)
{
    if (sqlite3_open(fileName.c_str(), &db) ) 
    {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
    } 
    else 
    {
        std::cout << "Opened database successfully" << std::endl;
    }
}

void dataBase::createTable(std::map<std::string, std::string> colsAndType)
{
    std::string sql = "CREATE TABLE SENSORS(";
    for (auto i = colsAndType.begin(); i != colsAndType.end(); ++i)
    {
        sql += (*i).first + " " + (*i).second + ",";
    }
    sql.pop_back();
    sql += ");";
    int status = sqlite3_exec(db, sql.c_str(), callBackEmpty, 0, &zErrMsg);
    if (status != SQLITE_OK)
    {
        std::cout << "SQL error:" << zErrMsg << std::endl;
        sqlite3_free(zErrMsg);
    }
    else
    {
        std::cout << "Table created successfully" << std::endl;
    }
}

void dataBase::insertInTable(std::map<std::string, std::string> colsAndData)
{
    std::string sql = "INSERT INTO SENSORS (";
    for (auto i = colsAndData.begin(); i != colsAndData.end(); ++i)
    {
        sql += (*i).first + ",";
    }
    sql.pop_back();
    sql += ") VALUES (";
    for (auto i = colsAndData.begin(); i != colsAndData.end(); ++i)
    {
        sql += (*i).second + ",";
    }
    sql.pop_back();
    sql += ");";
    if (sqlite3_exec(db, sql.c_str(), callBackEmpty, 0, &zErrMsg) != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, "Records created successfully\n");
    }
}
void dataBase::close(void)
{
    sqlite3_close(db);
}

void dataBase::getColumnsName()
{
    std::string sql = "SELECT * from SENSORS LIMIT 1";

    if (sqlite3_exec(db, sql.c_str(), callBackNameCols, 0, &zErrMsg) != SQLITE_OK) 
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, "Operation done successfully\n");
    }
}
void dataBase::printValFromRange(std::string cols, std::string typeVal, std::string dateTimeBeg, std::string dateTimeEnd)
{
    std::string sql = "SELECT ";
    sql += typeVal;
    sql += "(";
    sql += cols;
    sql += ") FROM SENSORS\nWHERE date BETWEEN '";
    sql += dateTimeBeg;
    sql += "'\nAND '";
    sql += dateTimeEnd;
    sql += "'\n";
    if (sqlite3_exec(db, sql.c_str(), callBackFindVal, 0, &zErrMsg) != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
}

void dataBase::printSensorName(void)
{
    for (size_t i = 0; i < dataBase::nameCols.size()-1; i++)
    {
        std::cout << i + 1 <<')'<< dataBase::nameCols[i] << ' ';
    }
    std::cout << std::endl;
}
