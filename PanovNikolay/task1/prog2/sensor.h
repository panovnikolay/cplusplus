#ifndef SENSOR_H
#define SENSOR_H
#include <string>

class Sensor {
public:
    virtual ~Sensor(){};
    virtual double getData() = 0;
private:
    double x;
    double y;
    std::string model;
    std::string installDate;
};

class HumiditySensor : public Sensor {
public:
    double getData();
private:
    unsigned int humidity = 50;
};

class TemperatureSensor : public Sensor {
public:
    double getData();
private:
    double temperature = 20.0;
};

#endif //SENSOR_H
